---
version: 2
type de projet: Projet de Bachelor
année scolaire: 2020/2021
titre: Interface sans écran pour player audio
mandants:
  - Christoph Walker
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
mots-clé:
  - Android
  - Hardware-UI
  - Audio-player
  - Systèmes embarqués

langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.4\textwidth,height=\textheight]{img/headphone.jpg}
\end{center}
```

## Contexte

Les parents utilisent une app d’un appareil Android pour programmer les raccourcis media (par exemple VLC, Spotify, Youtube, Deezer, etc.) qui sont à lancer avec un contenu prédéfini (titre, playlist, radio) lorsque leur enfant utilise son interface sans écran.

Les touts petits d’aujourd’hui ont la chance d’avoir accès à toute l’information du monde à travers internet, qui leur est mis à disposition par leurs parents. Les enfants en bas âge devraient éviter au maximum les écrans, ce qui fait que les parents sont beaucoup mis à contribution pour choisir la musique ou l’histoire à écouter pour leurs bambins.

## Description

L'idée de ce projet est de créer une interface utilisateur sans écran qui permettra aux enfants
de démarrer des raccourcis audio sur un appareil Android.

## Objectifs/Tâches

- Faire l’état des lieux sur le matériel et les applications de "player audio" pour enfants déjà disponibles sur le marché. Voici des exemples de produits pour enfants existants, dont certains avec écran : Boxine Toniebox, Tigermedia Tigerbox, Winzki Hörbert, Ocarina MP3 player.
- Créer un design hardware d'une interface matérielle suffisamment solide et simple pour les plus petits enfants, mais pouvant piloter un grand nombre de liens pour les plus grands. L'interface sera dépourvue d'écran, donc prévoir des boutons poussoirs, des tags RFID, des roulettes (ou d'autres idées innovantes). L'interface matérielle doit permettre de contrôler une application Android.
- Développer une application Android permettant
    - aux parents de configurer une liste de liens audio et, pour chaque lien, l’interface hardware qui démarre ce lien.
    - aux enfants de démarrer un lien à l’aide du hardware choisi, de naviguer les pistes et de mettre en pause.
- Concevoir et réaliser un prototype fonctionnel répondant au cahier des charges.
- Rédiger une documentation adéquate.
